const Web3 = require('web3');

export const web3 = new Web3(process.env.ETH_GATEWAY);
