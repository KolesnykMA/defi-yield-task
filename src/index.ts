import 'dotenv/config';
import {analyzeEthereumWalletInPeriod} from "./logic";
import {ethereumWalletAddress, otherWalletTokenAddresses} from "./config/config";

export const analyzeWallets = async () => {
    await analyzeEthereumWalletInPeriod(ethereumWalletAddress, otherWalletTokenAddresses);
}

(async () => {
    try {
        await analyzeWallets();
    } catch (error: any) {
        console.log('Internal error. ', error.messages);
    }
})();
