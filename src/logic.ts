import {web3} from "./web3/client";
import {abiErc20} from "./abi/erc20";
import {Asset, WalletInformation} from "./types";
import {saveWalletInformation} from "./persist/write";

export const analyzeEthereumWalletInPeriod = async (
    ethereumWalletAddress: string,
    otherWalletTokenAddresses: string[],
    pauseInterval = 60,
) => {
    await analyzeEthereumWallet(ethereumWalletAddress, otherWalletTokenAddresses);
    console.log('Analyzed wallet information ', ethereumWalletAddress);
    setTimeout(async () => await analyzeEthereumWalletInPeriod(ethereumWalletAddress, otherWalletTokenAddresses), pauseInterval * 1000);
}

const analyzeEthereumWallet = async (ethereumWalletAddress: string, otherWalletTokenAddresses: string[]) => {
    const assets: Asset[] = [];

    const ethereumBalance = await web3.eth.getBalance(ethereumWalletAddress);
    console.log('Fetched ethereum balance ', ethereumWalletAddress);

    assets.push({
        name: 'Ethereum',
        symbol: 'ETH',
        balance: web3.utils.fromWei(ethereumBalance),
    });

    await Promise.all(otherWalletTokenAddresses.map(async (tokenAddress) => {
        const contract = new web3.eth.Contract(abiErc20, tokenAddress);
        assets.push(await getTokenInfoFromContract(tokenAddress, ethereumWalletAddress, contract));
        console.log('Fetched token balance ', tokenAddress);
    }))

    const walletInformation: WalletInformation = {
        walletAddress: ethereumWalletAddress,
        createAt: new Date().toISOString(),
        assets
    };

    await saveWalletInformation(walletInformation);
    console.log('Saved wallet information to json');
}

const getTokenInfoFromContract = async (
    tokenAddress: string,
    ethereumWalletAddress: string,
    contract: any
): Promise<Asset> => {
    let name = 'undefined';
    let symbol = 'undefined';
    let decimals = 0;

    const tokenBalance = await contract.methods.balanceOf(ethereumWalletAddress).call();

    try {
        name = await contract.methods.name().call();
    } catch (error: any) {
        console.log("Error when fetching token name. ", error.message);
    }

    try {
        decimals = await contract.methods.decimals().call();
    } catch (error: any) {
        console.log("Error when fetching token decimals. ", error.message);
    }

    try {
        symbol = await contract.methods.symbol().call();
    } catch (error: any) {
        console.log("Error when fetching token symbol. ", error.message);
    }

    return {
        tokenAddress,
        name,
        symbol,
        balance: (tokenBalance / Math.pow(10, decimals)).toString()
    };
};
