import * as fs from 'fs';
import {WalletInformation} from "../types";

const savePath = 'analytics/'

export const saveWalletInformation = async (walletInformation: WalletInformation) => {
    const destination = savePath + walletInformation.walletAddress + '.json';

    fs.readFile(destination, function (err: any, data: any) {
        let json: WalletInformation[] = [];

        if (data) {
            json = JSON.parse(data);
        }

        json.push(walletInformation);

        fs.writeFile(destination, JSON.stringify(json), function(err: any){
            if (err) throw err;
        });
    })
}
