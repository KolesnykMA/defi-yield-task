jest.mock('./logic');

import {analyzeEthereumWalletInPeriod} from './logic';
import {analyzeWallets} from './index';

describe('analyzeWallets', () => {
    it("should call monitorEtherWallet", async () => {
        await analyzeWallets();

        expect(analyzeEthereumWalletInPeriod).toHaveBeenCalled();
    });
});
