export type WalletInformation = {
    walletAddress: string;
    createAt: string;
    assets: Asset[];
}

export type Asset = {
    tokenAddress?: string;
    name: string;
    symbol: string;
    balance: string;
}
