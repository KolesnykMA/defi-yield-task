# DefiYield test task
### By Kolesnyk Maksym

## How to start the project:
1. Install packages ```yarn```
2. Create new file in root folder `.env` and paste code from `.env-example` file in the new file. Inside `.env-example`
specify your own gateway
3. Run `yarn start`
4. Observe folder `analytics`
